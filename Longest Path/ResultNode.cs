﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Longest_Path
{
    class ResultNode : IComparable<ResultNode>
    {
        protected int elevation;
        protected int coordX;
        protected int coordY;
        protected MapNode parent;

        private ResultNode()
        {
        }

        public ResultNode(int elevation, int coordX, int coordY)
        {
            this.elevation = elevation;
            this.coordX = coordX;
            this.coordY = coordY;
            this.parent = null;
        }

        public ResultNode(int elevation, int coordX, int coordY, MapNode parent)
        {
            this.elevation = elevation;
            this.coordX = coordX;
            this.coordY = coordY;
            this.parent = parent;
        }

        public int getElevation()
        {
            return elevation;
        }

        public int getX()
        {
            return coordX;
        }

        public int getY()
        {
            return coordY;
        }

        public int CompareTo(ResultNode other)
        {
            if (elevation == other.elevation)
            {
                return 0;
            }
            else if (elevation < other.elevation)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public MapNode getParent()
        {
            return parent;
        }

        public void setParent(MapNode parent)
        {
            this.parent = parent;
        }
    }
}
