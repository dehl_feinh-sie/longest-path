﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Longest_Path
{
    class MapNode
    {
        protected int elevation;
        protected int x;
        protected int y;
        protected bool visited;
        protected MapNode parent;

        public MapNode()
        {
            this.elevation = 0;
            this.visited = false;
        }

        public void setElevation(int p)
        {
            elevation = p;
        }

        public int getElevation()
        {
            return elevation;
        }

        public void setX(int x)
        {
            this.x = x;
        }

        public void setY(int y)
        {
            this.y = y;
        }

        public int getX()
        {
            return x;
        }

        public int getY()
        {
            return y;
        }

        public void setVisited(bool visited)
        {
            this.visited = visited;
        }

        public bool getVisited()
        {
            return visited;
        }

        public void setParent(MapNode parent)
        {
            this.parent = parent;
        }

        public MapNode getParent()
        {
            return parent;
        }
    }
}
