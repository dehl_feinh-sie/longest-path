﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Longest_Path
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "map.txt";
            Graph map = new Graph(filename);
            map.findLongestPath();
            map.writeLongestPath();

            Console.WriteLine("Beliebige Taste drücken...");
            Console.ReadKey();
        }
    }
}
