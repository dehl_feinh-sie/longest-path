﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace Longest_Path
{
    class Graph
    {
        protected int ELEVATION_MAX = 1500;
        protected int ELEVATION_MIN = 0;

        protected int mapWidth;
        protected int mapHeight;
        public MapNode[][] map;

        List<ResultNode> valleys;
        List<ResultNode> summits;
        List<ResultNode> longestPath;
        List<ResultNode> currentPath;

        private Graph()
        {
        }

        public Graph(string filename)
        {
            StreamReader mapFile;
            string mapRow;
            string[] buffer;

            valleys = new List<ResultNode>();
            summits = new List<ResultNode>();
            longestPath = new List<ResultNode>();
            currentPath = new List<ResultNode>();

            try
            {
                mapFile = new StreamReader(filename);
                mapRow = mapFile.ReadLine();

                // Read first line of the map file.
                buffer = mapRow.Split(new char[] { ' ' });
                mapWidth = Convert.ToInt32(buffer[0]);
                mapHeight = Convert.ToInt32(buffer[1]);

                // Initialize map array
                map = new MapNode[mapWidth][];

                for (int i = 0; i < mapWidth; i++)
                {
                    map[i] = new MapNode[mapHeight];

                    for (int j = 0; j < mapHeight; j++)
                    {
                        map[i][j] = new MapNode();
                    }
                }

                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                    {
                        map[x][y].setX(x);
                        map[x][y].setY(y);
                    }
                }

                // Read map data.
                {
                    int j = 0, i = 0;
                    while (((mapRow = mapFile.ReadLine()) != null) && (j < mapHeight))
                    {
                        buffer = mapRow.Split(new char[] { ' ' });

                        for (i = 0; i < mapWidth; i++)
                        {
                            map[i][j].setElevation(Convert.ToInt32(buffer[i]));
                        }

                        j++;
                    }
                }

                mapFile.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public MapNode getNode(int x, int y)
        {
            return map[x][y];
        }

        public void setNodeElevation(int x, int y, int elevation)
        {
            map[x][y].setElevation(elevation);
        }

        public void writeMap()
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    Console.Write(map[x][y].getElevation() + " ");
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void writeMapVisited()
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    if (map[x][y].getVisited() == true)
                    {
                        Console.Write("1 ");
                    }
                    else
                    {
                        Console.Write("0 ");
                    }
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public void unvisitMap()
        {
            // Inefficient implementation
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    map[x][y].setVisited(false);
                }
            }
        }

        public bool isSummit(int x, int y)
        {
            if ((x == 0) && (y == 0))
            {
                // Is at north-western (top-left) border.
                if ((map[x][y].getElevation() >= map[x][y + 1].getElevation()) && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y == 0))
            {
                // Is at northern (top) border.
                if ((map[x][y].getElevation() >= map[x][y + 1].getElevation()) && (map[x][y].getElevation() >= map[x - 1][y].getElevation())
                    && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x == mapWidth - 1) && (y == 0))
            {
                // Is at north-eastern (top-right) border.
                if ((map[x][y].getElevation() >= map[x][y + 1].getElevation()) && (map[x][y].getElevation() >= map[x - 1][y].getElevation()))
                    return true;
            }
            else if ((x == 0) && (y > 0) && (y < mapHeight - 1))
            {
                // Is at the western (left) border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x][y + 1].getElevation())
                    && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y > 0) && (y < mapHeight - 1))
            {
                // Isn't at the border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x][y + 1].getElevation())
                    && (map[x][y].getElevation() >= map[x - 1][y].getElevation()) && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x == mapWidth - 1) && (y > 0) && (y < mapHeight - 1))
            {
                // Is at the eastern (right) border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x][y + 1].getElevation())
                    && (map[x][y].getElevation() >= map[x - 1][y].getElevation()))
                    return true;
            }
            else if ((x == 0) && (y == mapHeight - 1))
            {
                // Is at south-western (bottom-left) border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y == mapHeight - 1))
            {
                // Is at the southern (bottom) border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x - 1][y].getElevation())
                    && (map[x][y].getElevation() >= map[x + 1][y].getElevation()))
                    return true;
            }
            else if ((x == mapWidth - 1) && (y == mapHeight - 1))
            {
                // Is at south-eastern (bottom-right) border.
                if ((map[x][y].getElevation() >= map[x][y - 1].getElevation()) && (map[x][y].getElevation() >= map[x - 1][y].getElevation()))
                    return true;
            }

            return false;
        }

        protected void findAllSummits()
        {
            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    if (isSummit(x, y))
                    {
                        summits.Add(new ResultNode(map[x][y].getElevation(), x, y));
                    }
                }
            }
        }

        public void findLongestPath()
        {
            valleys.Clear();
            findAllSummits();

            foreach (ResultNode summit in summits)
            {
                findPath(summit.getX(), summit.getY(), null);
                unvisitMap();

                foreach (ResultNode valley in valleys)
                {
                    traverseFromValleyToSummit(map[valley.getX()][valley.getY()]);
                    currentPath.Reverse();

                    //writeCurrentPath();

                    if (currentPath.Count() > longestPath.Count())
                    {
                        longestPath = new List<ResultNode>(currentPath);
                        writeLongestPath();
                    }
                    else if (currentPath.Count() == longestPath.Count())
                    {
                        // Get the higher drop.
                        if ((currentPath.First().getElevation() - currentPath.Last().getElevation()) > (longestPath.First().getElevation() - longestPath.Last().getElevation()))
                        {
                            longestPath = new List<ResultNode>(currentPath);
                            writeLongestPath();
                        }
                    }

                    currentPath.Clear();
                }

                // Each summit can has another reachable valleys.
                valleys.Clear();
            }
        }

        protected void traverseFromValleyToSummit(MapNode node)
        {
            for (; node != null; node = node.getParent())
            {
                currentPath.Add(new ResultNode(node.getElevation(), node.getX(), node.getY()));
            }
        }

        public void findPath(int x, int y, MapNode parent)
        {
            List<ResultNode> directions = new List<ResultNode>(3);

            if (map[x][y].getVisited() == true)
            {
                return;
            }
            map[x][y].setVisited(true);
            map[x][y].setParent(parent);

            //writeMapVisited();

            //Which directions are possible
            if ((x == 0) && (y == 0))
            {
                // Is at north-western (top-left) border.
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y == 0))
            {
                // Is at northern (top) border.
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x == mapWidth - 1) && (y == 0))
            {
                // Is at north-eastern (top-right) border.
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x == 0) && (y > 0) && (y < mapHeight - 1))
            {
                // Is at the western (left) border.
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y > 0) && (y < mapHeight - 1))
            {
                // Isn't at the border.
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x == mapWidth - 1) && (y > 0) && (y < mapHeight - 1))
            {
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
                if (map[x][y].getElevation() > map[x][y + 1].getElevation())
                    directions.Add(new ResultNode(map[x][y + 1].getElevation(), x, y + 1));
            }
            else if ((x == 0) && (y == mapHeight - 1))
            {
                // Is at south-western (bottom-left) border.
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
            }
            else if ((x > 0) && (x < mapWidth - 1) && (y == mapHeight - 1))
            {
                // Is at the southern (bottom) border.
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
                if (map[x][y].getElevation() > map[x + 1][y].getElevation())
                    directions.Add(new ResultNode(map[x + 1][y].getElevation(), x + 1, y));
            }
            else if ((x == mapWidth - 1) && (y == mapHeight - 1))
            {
                // Is at south-eastern (bottom-right) border.
                if (map[x][y].getElevation() > map[x][y - 1].getElevation())
                    directions.Add(new ResultNode(map[x][y - 1].getElevation(), x, y - 1));
                if (map[x][y].getElevation() > map[x - 1][y].getElevation())
                    directions.Add(new ResultNode(map[x - 1][y].getElevation(), x - 1, y));
            }

            if (directions.Count() == 0)
            {
                // If is in any direction no field with lower elevation, then it's a valley.
                // Valleys mean it's end of the downhill ride.
                valleys.Add(new ResultNode(map[x][y].getElevation(), x, y, parent));
                return;
            }

            // For the purpose of getting the lowest drop to a neighbor field.
            // On a route with low drops is the ride especially long.
            directions.Sort();

            //traverse graph
            foreach (ResultNode e in directions)
            {
                findPath(e.getX(), e.getY(), map[x][y]);
            }
        }

        public void writeSummits()
        {
            Console.WriteLine("SUMMITS:");
            foreach (ResultNode node in summits)
            {
                Console.WriteLine("({0}, {1}) -> {2}", node.getX(), node.getY(), node.getElevation());
            }
            Console.WriteLine();
        }

        protected void writeCurrentPath()
        {
            Console.WriteLine("CURRENT PATH:");
            Console.WriteLine("Drop:   " + (currentPath.First().getElevation() - currentPath.Last().getElevation()));
            Console.WriteLine("Length: " + (currentPath.Count()));

            foreach (ResultNode node in currentPath)
            {
                Console.WriteLine("({0}, {1}) -> {2}", node.getX(), node.getY(), node.getElevation());
            }
            Console.WriteLine();
        }

        public void writeLongestPath()
        {
            Console.WriteLine("LONGEST PATH:");
            Console.WriteLine("Drop:   " + (longestPath.First().getElevation() - longestPath.Last().getElevation()));
            Console.WriteLine("Length: " + (longestPath.Count()));

            foreach (ResultNode node in longestPath)
            {
                Console.WriteLine("({0}, {1}) -> {2}", node.getX(), node.getY(), node.getElevation());
            }
            Console.WriteLine();
        }
    }
}
